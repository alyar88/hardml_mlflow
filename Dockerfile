FROM python:3.8
WORKDIR /usr/src/app
COPY . .
RUN pip install -r requirements.txt
HEALTHCHECK --interval=1s --retries=5 \
  CMD curl -f 'http://localhost:5005/get_source_iris_pred'

CMD ["python3", "main.py"]