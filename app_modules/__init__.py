import json
import os
import mlflow, mlflow.sklearn

from app_modules import db


def init_secret():
    with open('secret.json') as f:
        db.secret_number = json.load(f)['secret_number']


def init_thresholds():
    with open('thresholds.json') as f:
        db.thresholds = json.load(f)


def init_model():
    mlflow.set_tracking_uri(os.environ["MLFLOW_TRACKING_URI"])
    print(os.environ["MLFLOW_TRACKING_URI"])
    db.model = mlflow.sklearn.load_model('models:/iris_sklearn/staging')


init_thresholds()
init_model()
