from flask import Flask, jsonify, request

from app_modules import db

app = Flask(__name__)


@app.get('/get_source_iris_pred')
def predict_float():
    args = request.args
    sepal_length = args.get('sepal_length', type=float, default=0.)
    sepal_width = args.get('sepal_width', type=float, default=0.)
    preds = db.model.predict([[sepal_length, sepal_width]])

    return jsonify({'prediction': float(preds[0])})


@app.get('/get_string_iris_pred')
def predict_string():
    return {}, 501


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5005)
